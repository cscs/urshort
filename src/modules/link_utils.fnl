;; modules/link_utils.fnl
;; Provides a collection of utilities for working with link IDs and URIs

;; -------------------------------------------------------------------------- ;;
;; Dependencies

(local hashids (require :hashids))

;; -------------------------------------------------------------------------- ;;
;; Local modules

(local config (require :modules.config))
(local links_db (require :modules.links_db))

;; -------------------------------------------------------------------------- ;;
;; Accepted character sets

(local base62_accepted_char_set "[%a%d]+")
(local scheme_accepted_char_set "[%a%d%.%-]+")

;; -------------------------------------------------------------------------- ;;
;; Module definition

(local link_utils {})

(fn link_utils.normalize_uri [uri]
  "Return `uri` normalized. This will add a scheme (default http) if one is not present.)"
  (if (string.find uri (.. "^" scheme_accepted_char_set "://"))
      uri
      (.. "http://" uri)))

(fn link_utils.valid_link_id? [link_id]
  "Return true if `link_id` is a valid base62 link ID, otherwise return false."
  (if (string.find link_id (.. "^" base62_accepted_char_set "$"))
      true
      false))

(fn link_utils.valid_link_dest? [link_dest]
  "Return true if 'link_dest' is a valid URI, otherwise return false."
  (if (string.find link_dest (.. "^" scheme_accepted_char_set "://.+$"))
      true
      false))

;; -------------------------------------------------------------------------- ;;
;; Return module

link_utils
