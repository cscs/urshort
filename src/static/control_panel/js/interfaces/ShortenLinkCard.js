/**
 * interfaces/ShortenLinkCard.js
 *
 * @file Provides interface to state-safe functions of ShortenLinkCard controller
 *
 * @author Jessie Hildebrandt
 */

/* -------------------------------------------------------------------------- */
/* Local module imports */

import { ShortenLinkCard as Controller } from '../controllers/ShortenLinkCard.js';

/* -------------------------------------------------------------------------- */
/* Interface implementation */

/**
 * Interface for ShortenLinkCard controller
 */
const ShortenLinkCard = {

    /* ---------------------------------- */
    /* refresh */

    /**
     * Trigger a full refresh and redraw of the component
     */
    refresh()
    {
        Controller.refresh();
    }

}

/* -------------------------------------------------------------------------- */
/* Export */

export { ShortenLinkCard };
