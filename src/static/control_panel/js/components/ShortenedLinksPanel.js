/**
 * components/ShortenedLinksPanel.js
 *
 * @file Provides a "Shorten A Link" component that allows the user to shorten links
 *
 * @author Jessie Hildebrandt
 */

/* -------------------------------------------------------------------------- */
/* Local module imports */

import { ShortenedLinksPanel as Controller } from '../controllers/ShortenedLinksPanel.js';

import { LinkList } from '../components/LinkList.js';

import { filterBase62Input } from '../utils/utils.js';

/* -------------------------------------------------------------------------- */
/* Component implementation */

const ShortenedLinksPanel = {

    /* ---------------------------------- */
    /* oninit */

    /**
     * Called when the component is initialized
     */
    oninit: () => {
        Controller.init();
        Controller.updateLinkList();
    },

    /* ---------------------------------- */
    /* onremove */

    /**
     * Called before component element is removed from the DOM
     */
    onremove: () => {
        Controller.reset();
    },

    /* ---------------------------------- */
    /* view */

    /**
     * Called whenever the component is drawn
     *
     * @param {m.Vnode} vnode - The Vnode representing the component
     *
     * @returns {m.Vnode} - The Vnode or Vnode tree to be rendered
     */
    view: ( { attrs } ) => {
        return m( '.ShortenedLinksPanel.column.is-6-desktop', [
            m( '.panel', [
                m( 'header.panel-heading', 'Shortened links' ),
                m( 'section.panel-search', [
                    m( '.control.has-icons-left', {
                            class: Controller.isWaiting ? 'is-loading' : null,
                    }, [
                        m( 'input.input', {
                            type: 'text',
                            disabled: attrs.disabled,
                            placeholder: 'Search',
                            onkeypress: ( event ) => filterBase62Input( event ),
                            onpaste: ( event ) => filterBase62Input( event ),
                            oninput: ( event ) => Controller.updateSearchQuery( event.target.value )
                        } ),
                        m( 'span.icon.is-medium.is-left', [
                            m( 'i.fa.fa-search' )
                        ] )
                    ] )
                ] ),
                m( 'section.panel-tabs', [
                    m( 'a', {
                        class: Controller.onlyShowNamed ? null : 'is-active',
                        onclick: () => Controller.setOnlyShowNamed( false )
                    }, 'All' ),
                    m( 'a', {
                        class: Controller.onlyShowNamed ? 'is-active' : null,
                        onclick: () => Controller.setOnlyShowNamed( true )
                    }, 'Named' )
                ] ),
                m( LinkList, {
                    links: Controller.links,
                    highlight: Controller.searchQuery
                } )
            ] )
        ] );
    }

};

/* -------------------------------------------------------------------------- */
/* Export */

export { ShortenedLinksPanel };
