/**
 * components/LinkDeletionModal.js
 *
 * @file Provides a "Link deletion" modal component that allows the user to delete a link
 *
 * @author Jessie Hildebrandt
 */

/* -------------------------------------------------------------------------- */
/* Local module imports */

import { LinkDeletionModal as Controller } from '../controllers/LinkDeletionModal.js';

/* -------------------------------------------------------------------------- */
/* Helper functions */

/* ---------------------------------- */
/* formatCountdownValue */

/**
 * Format and return a countdown timer value into a pretty number
 *
 * @param {number} value - The countdown timer value to format
 *
 * @returns {string} The pretty formatted number
 */
function formatCountdownValue( value )
{
    switch ( value )
    {
        case 2:
        return '➌ ';
        case 1:
        return '➋ ';
        case 0:
        return '➊ ';
        default:
        return '… ';
    }
}

/* -------------------------------------------------------------------------- */
/* Component implementation */

const LinkDeletionModal = {

    /* ---------------------------------- */
    /* oninit */

    /**
     * Called when the component is initialized
     *
     * @param {m.Vnode} vnode - The Vnode representing the component
     */
    oninit: ( { attrs } ) => {
        Controller.init();
        Controller.load( attrs.linkID );
    },

    /* ---------------------------------- */
    /* onremove */

    /**
     * Called before component element is removed from the DOM
     */
    onremove: () => {
        Controller.reset();
    },

    /* ---------------------------------- */
    /* view */

    /**
     * Called whenever the component is drawn
     *
     * @returns {m.Vnode} - The Vnode or Vnode tree to be rendered
     */
    view: ( { attrs } ) => {
        return m( '.LinkDeletionModal.modal-card', [
            m( 'header.modal-card-head', [
                m( 'p.modal-card-title', 'Link deletion' ),
                m( 'button.delete', {
                    disabled: Controller.isWaiting,
                    onclick: Controller.close
                } )
            ] ),
            m( 'section.modal-card-body', [
                m( '.field', [
                    m( '.control.has-icons-left', [
                        m( 'input.input.is-static', {
                            type: 'text',
                            readonly: true,
                            value: attrs.linkID
                        } ),
                        m( 'span.icon.is-left', [
                            m( 'i.fa.fa-tag' )
                        ] )
                    ] )
                ] ),
                m( '.field', [
                    m( '.control.has-icons-left', {
                        class: Controller.isWaiting ? 'is-loading' : null
                    }, [
                        m( 'input.input.is-static', {
                            type: 'text',
                            placeholder: 'Destination',
                            readonly: true,
                            value: Controller.linkDest
                        } ),
                        m( 'span.icon.is-left', [
                            m( 'i.fa.fa-link' )
                        ] )
                    ] )
                ] ),
                m( '.field.has-text-centered', [
                    m( 'small', 'Are you sure that you want to delete this link?' )
                ] ),
                m( '.field.is-grouped.is-justify-content-center', [
                    m( '.control', [
                        m( 'button.button.is-link.is-outlined', {
                            disabled: Controller.isWaiting,
                            onclick: () => Controller.openOptionsModal( attrs.linkID )
                        }, 'Go back' )
                    ] ),
                    m( '.control', [
                        m( 'button.button.is-danger.is-outlined', {
                            disabled: Controller.isWaiting || Controller.isCountingDown,
                            onclick: () => Controller.tryDelete( attrs.linkID )
                        }, `${ Controller.isCountingDown ? formatCountdownValue( Controller.deletionCountdown ) : '' }Confirm` )
                    ] )
                ] )
            ] )
        ] );
    }

}

/* -------------------------------------------------------------------------- */
/* Export */

export { LinkDeletionModal };
