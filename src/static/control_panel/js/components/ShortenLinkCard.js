/**
 * components/ShortenLinkCard.js
 *
 * @file Provides a "Shorten a link" component that allows the user to shorten links
 *
 * @author Jessie Hildebrandt
 */

/* -------------------------------------------------------------------------- */
/* Local module imports */

import { ShortenLinkCard as Controller } from '../controllers/ShortenLinkCard.js';

import { filterBase62Input } from '../utils/utils.js';

/* -------------------------------------------------------------------------- */
/* Helper functions */

/* ---------------------------------- */
/* controlClassFromFieldState */

/**
 * Return the appropriate CSS class name for a control element to reflect `fieldState`
 *
 * @param {string} fieldState - The state of the field wrapped by the control element
 *
 * @returns {?string} The CSS class name for the control element
 */
function controlClassFromFieldState( fieldState )
{
    switch( fieldState )
    {
        case Controller.FIELD_STATE.VALIDATING:
        return 'is-loading';
        default:
        return null;
    }
};

/* ---------------------------------- */
/* inputClassFromFieldState */

/**
 * Return the appropriate CSS class name for an input element to reflect `fieldState`
 *
 * @param {string} fieldState - The state of the field wrapped by the input element
 *
 * @returns {?string} The CSS class name for the input element
 */
function inputClassFromFieldState( fieldState )
{
    switch( fieldState )
    {
        case Controller.FIELD_STATE.VALID:
        return 'is-success';
        case Controller.FIELD_STATE.INVALID:
        return 'is-danger';
        default:
        return null;
    }
};

/* -------------------------------------------------------------------------- */
/* Component implementation */

const ShortenLinkCard = {

    /* ---------------------------------- */
    /* oninit */

    /**
     * Called when the component is initialized
     */
    oninit: () => {
        Controller.init();
    },

    /* ---------------------------------- */
    /* onremove */

    /**
     * Called before component element is removed from the DOM
     */
    onremove: () => {
        Controller.reset();
    },

    /* ---------------------------------- */
    /* view */

    /**
     * Called whenever the component is drawn
     *
     * @returns {m.Vnode} - The Vnode or Vnode tree to be rendered
     */
    view: ( { attrs } ) => {
        return m( '.ShortenLinkCard.column.is-6-desktop', [
            m( '.card', [
                m( 'header.card-header', [
                    m( 'p.card-header-title', 'Shorten a link' )
                ] ),
                m( 'section.card-content', [
                    m( '.field', [
                        m( '.control.has-icons-left', {
                            class: controlClassFromFieldState( Controller.linkNameState )
                        }, [
                            m( 'input.input', {
                                class: inputClassFromFieldState( Controller.linkNameState ),
                                type: 'text',
                                disabled: attrs.disabled,
                                placeholder: 'Name (optional)',
                                maxlength: 32,
                                value: Controller.linkName,
                                onkeypress: ( event ) => filterBase62Input( event ),
                                onpaste: ( event ) => filterBase62Input( event ),
                                oninput: ( event ) => Controller.updateLinkName( event.target.value ),
                                onkeydown: ( event ) => {
                                    if ( Controller.canShorten() && event.key === 'Enter' )
                                    {
                                        Controller.tryShorten();
                                    }
                                }
                            } ),
                            m( 'span.icon.is-left', [
                                m( 'i.fa.fa-tag' )
                            ] )
                        ] )
                    ] ),
                    m( '.field', [
                        m( '.control.has-icons-left', {
                            class: controlClassFromFieldState( Controller.linkDestState )
                        }, [
                            m( 'input.input', {
                                class: inputClassFromFieldState( Controller.linkDestState ),
                                type: 'text',
                                disabled: attrs.disabled,
                                placeholder: 'Destination',
                                value: Controller.linkDest,
                                oninput: ( event ) => Controller.updateLinkDest( event.target.value ),
                                onkeydown: ( event ) => {
                                    if ( Controller.canShorten() && event.key === 'Enter' )
                                    {
                                        Controller.tryShorten();
                                    }
                                }
                            } ),
                            m( 'span.icon.is-left', [
                                m( 'i.fa.fa-link' )
                            ] )
                        ] )
                    ] ),
                    m( '.field.is-grouped.is-justify-content-center', [
                        m( 'button.button.is-link.is-outlined', {
                            class: Controller.isWaiting ? 'is-loading' : null,
                            disabled: attrs.disabled || !Controller.canShorten(),
                            onclick: () => {
                                if ( Controller.canShorten() )
                                {
                                    Controller.tryShorten()
                                }
                            }
                        }, 'Shorten link' )
                    ] )
                ] )
            ] )
        ] );

    }

};

/* -------------------------------------------------------------------------- */
/* Export */

export { ShortenLinkCard };
