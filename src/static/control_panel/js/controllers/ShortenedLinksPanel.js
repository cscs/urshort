/**
 * controllers/ShortenedLinksPanel.js
 *
 * @file Provides controller for ShortenedLinksPanel component
 *
 * @author Jessie Hildebrandt
 */

/* -------------------------------------------------------------------------- */
/* Local module imports */

import { delayedInterruptableRequest } from '../utils/utils.js';

/* -------------------------------------------------------------------------- */
/* Controller implementation */

const ShortenedLinksPanel = {

    /* ---------------------------------- */
    /* Enums */

    FIELD_STATE: {
        EMPTY: '',
        VALIDATING: 'validating',
        VALID: 'valid',
        INVALID: 'invalid'
    },

    /* ---------------------------------- */
    /* init */

    /**
     * Initializes the controller
     */
    init()
    {

        ShortenedLinksPanel.searchQuery = '';

        ShortenedLinksPanel.links = [];

        ShortenedLinksPanel.onlyShowNamed = false;
        ShortenedLinksPanel.isWaiting = false;

    },

    /* ---------------------------------- */
    /* reset */

    /**
     * Resets the controller
     */
    reset()
    {
        ShortenedLinksPanel.init();
    },

    /* ---------------------------------- */
    /* refresh */

    /**
     * Trigger a full refresh and redraw of the component
     */
    refresh()
    {
        ShortenedLinksPanel.updateLinkList();
    },

    /* ---------------------------------- */
    /* updateLinkList */

    /**
     * Update the list of stored links by querying the links API with the current search query (if present).
     * If a delay is specified, the request will be delayed and is interruptable to allow e.g. the user
     * time to continue typing in their search query.
     *
     * @param {number} delayMilliseconds - How long to wait before making the request to the API
     */
    async updateLinkList( delayMilliseconds = 0 )
    {

        // Signal to the component that activity is occurring
        ShortenedLinksPanel.isWaiting = true;
        m.redraw();

        // Submit a request to the API with our search query to retrieve a new list of links
        try
        {
            const query = `${ ShortenedLinksPanel.searchQuery }${ ShortenedLinksPanel.onlyShowNamed ? '?custom=true' : '' }`;
            const results = await delayedInterruptableRequest( '/shorten/api/links/', query, delayMilliseconds );
            if ( results )
            {
                ShortenedLinksPanel.links = results;
            }
            else
            {
                ShortenedLinksPanel.links = [];
            }
        }
        catch
        {
            ShortenedLinksPanel.links = [];
        }

        // We're done our work
        ShortenedLinksPanel.isWaiting = false;
        m.redraw();

    },

    /* ---------------------------------- */
    /* updateSearchQuery */

    /**
     * Update the search query stored in the controller
     *
     * @param {string} value - The value to update the search query to
     */
    updateSearchQuery( value )
    {
        ShortenedLinksPanel.searchQuery = value;
        ShortenedLinksPanel.updateLinkList( 200 );
    },

    /* ---------------------------------- */
    /* setFilterNamed */

    /**
     * Set the status of the "only named" filter in the controller
     *
     * @param {boolean} value - Whether the "only named" filter should be enabled
     */
    setOnlyShowNamed( value )
    {
        ShortenedLinksPanel.onlyShowNamed = value;
        ShortenedLinksPanel.updateLinkList();
    }

};

/* -------------------------------------------------------------------------- */
/* Export */

export { ShortenedLinksPanel };
