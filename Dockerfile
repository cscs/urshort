# simple-shortener Dockerfile
# Produces an image (Debian-based) with all required facilities to run simple-shortener

FROM debian:buster-slim

LABEL maintainer='Jessie Hildebrandt <jessieh@jessieh.net>'

ENV PORT 80
WORKDIR /public

# Install required packages for building luarocks and lua rocks
RUN apt-get update && apt-get install --no-install-recommends -y \
    ca-certificates \
    cmake \
    gcc \
    git \
    luajit \
    liblua5.1-0-dev \
    libqrencode-dev \
    libsqlite3-dev \
    libssl-dev \
    make \
    tar \
    unzip \
    wget \
    && rm -rf /var/lib/apt/lists/*

# Fetch, build, and install luarocks
RUN wget https://luarocks.org/releases/luarocks-3.9.1.tar.gz && \
    tar xf luarocks-3.9.1.tar.gz && \
    cd luarocks-3.9.1 && \
    ./configure && make && make install && \
    cd .. && \
    rm -rf luarocks-3.9.1.tar.gz luarocks-3.9.1/

# Install required rocks
RUN luarocks install basexx && \
    luarocks install ezenv && \
    luarocks install fennel && \
    luarocks install hashids && \
    luarocks install lume && \
    luarocks install otp && \
    luarocks install qrprinter && \
    luarocks install sqlite && \
    luarocks install turbo

COPY ./src .

ENTRYPOINT ["luajit", "init.lua"]
